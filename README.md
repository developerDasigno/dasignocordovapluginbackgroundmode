
<p align="left">
    <b><a href="https://github.com/katzer/cordova-plugin-background-mode/tree/example">SAMPLE APP</a> :point_right:</b>
</p>

Cordova Background Plugin [![npm version](https://badge.fury.io/js/cordova-plugin-background-mode.svg)](http://badge.fury.io/js/cordova-plugin-background-mode) [![Build Status](https://travis-ci.org/katzer/cordova-plugin-background-mode.svg?branch=master)](https://travis-ci.org/katzer/cordova-plugin-background-mode) [![codebeat badge](https://codebeat.co/badges/49709283-b313-4ced-8630-f520baaec7b5)](https://codebeat.co/projects/github-com-katzer-cordova-plugin-background-mode)
=========================

Plugin for the [Cordova][cordova] framework to perform infinite background execution.

Most mobile operating systems are multitasking capable, but most apps dont need to run while in background and not present for the user. Therefore they pause the app in background mode and resume the app before switching to foreground mode.
The system keeps all network connections open while in background, but does not deliver the data until the app resumes.

#### Store Compliance
Infinite background tasks are not official supported on most mobile operation systems and thus not compliant with public store vendors. A successful submssion isn't garanteed.

Use the plugin by your own risk!


## Supported Platforms
- __Android/Amazon FireOS__
- __Browser__
- __iOS__
- __Windows__ _(see #222)_


## Git origin
Tomado del git original [katzer](https://github.com/katzer/cordova-plugin-background-mode)

## Cambios latotuga
- Nuevas políticas en iOS.
- No permitir la mezcla de audio en background
- No emitir notificaciones innecesarias
- No permitir que la aplicación se muestre cuando el celular se bloquea
